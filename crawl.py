#!/usr/bin/env python2
# -*- coding: utf-8 -*-
from collections import Counter

# import crawlers
from EmerceCrawler import EmerceCrawler
from FrankWatchingCrawler import FrankWatchingCrawler
from KarelGeenenCrawler import KarelGeenenCrawler
from MarketingFactsCrawler import MarketingFactsCrawler
from MailCampCrawler import MailCampCrawler

# init crawlers
em = EmerceCrawler([
    'https://www.emerce.nl/channel/emailmarketing',
    'https://www.emerce.nl/channel/emailmarketing/page/2',
    'https://www.emerce.nl/channel/emailmarketing/page/3'
])
fw = FrankWatchingCrawler([
    'https://www.frankwatching.com/tag/e-mailmarketing/'
])
kg = KarelGeenenCrawler([
    'https://www.karelgeenen.nl/category/email-marketing/',
    'https://www.karelgeenen.nl/category/email-marketing/page/2/',
    'https://www.karelgeenen.nl/category/marketing-automation/'
])
mf = MarketingFactsCrawler([
    'https://www.marketingfacts.nl/rubrieken/e_mail_marketing',
    'https://www.marketingfacts.nl/rubrieken/e_mail_marketing/P15',
    'https://www.marketingfacts.nl/rubrieken/e_mail_marketing/P30'
])
mc = MailCampCrawler([
    'https://www.mailcamp.nl/kennis/blog'
])

# fw needs to start it's driver
fw.start()

# get all articles
articles = em.articles() + fw.articles() + kg.articles() + mf.articles() + mc.articles()

# prepare a counter for all words
commons = Counter()

# loop over all articles
for article in articles:
    commons += article.common_words()

# print out all commons
print commons.most_common(20)

# close the fw driver
fw.close()
