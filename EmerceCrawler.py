from Crawler import Crawler
from Article import Article
import threading

""" EmerceParser class

This class provides an interface for retrieving email marketing articles
from their blog.

@author         Tycho Atsma  <tycho.atsma@copernica.com>
@file           EmerceCrawler.py
@documentation  public
"""
class EmerceCrawler(Crawler):

    def links(self, url):
        """
        Get all links on a given url

        @param   string  the url to crawl
        @return  array
        """
        soup = self.soupify(url)
        
        # find all articles
        return [article.find('a')['href'] for article in soup.find_all('article', {"data-channel":"emailmarketing:E-mailmarketing"})]

    def article(self, url):
        """
        Get an article's contents of a given url

        @param   string  the url to crawl
        @return  void
        """
        print "start crawling %s on thread: %s" % (url, threading.current_thread().name)
        soup = self.soupify(url)

        # find the main article container
        container = soup.find('article', {"class":"article"})

        # get the title
        title = container.find('h1', {"class":"article-title"}).get_text().encode('utf-8')

        # get the content container
        content = container.find('div', {"class":"entry-content"})

        # parse the content
        text = ' '.join([p.get_text().encode('utf-8') for p in content.find_all('p')])

        # return the article
        self._articles.append(Article(title, text))
