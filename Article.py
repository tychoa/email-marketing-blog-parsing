from ContentParser import ContentParser

""" Article class

An article class that provides an interface to easily retrieve
article data.

@author         Tycho Atsma  <tycho.atsma@copernica.com>
@file           Article.py
@documentation  public
"""
class Article:
    
    def __init__(self, title, content):
        """
        Initialize the article

        @param   string  the title of the article
        @param   string  the content of the article
        @return  void
        """
        self._title = title
        self._content = content
        self._parser = ContentParser(content)

    def title(self):
        """
        Return the article title

        @return  string
        """
        return self._title

    def content(self):
        """
        Return the article content

        @return  string
        """
        return self._content

    def common_words(self):
        """
        Return the most common words

        @return  Counter
        """
        return self._parser.count_words()
