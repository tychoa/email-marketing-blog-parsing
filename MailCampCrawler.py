from Crawler import Crawler
from Article import Article
import threading

""" MailCampCrawler class

This class provides an interface for retrieving email marketing articles
from their blog.

@author         Tycho Atsma  <tycho.atsma@copernica.com>
@file           MailCampCrawler.py
@documentation  public
"""
class MailCampCrawler(Crawler):

    def links(self, url):
        """
        Get all links from a given url

        @param   string  the url to retrieve the links from
        @return  array
        """
        soup = self.soupify(url)
        
        # find all posts
        posts = soup.find_all('article', {"class":"post"})

        # return all links
        return [link.find('a')['href'] for link in posts]

    def article(self, url):
        """
        Get the contents of an article from a given url

        @param   string  the url to retrieve the content from
        @return  void
        """
        print "start crawling %s on thread: %s" % (url, threading.current_thread().name)
        soup = self.soupify(url)

        # find the post
        post = soup.find('article', {"class":"post"})

        # get the title
        title = post.find('h1', {"class":"entry-title"})
        
        # valid title?
        if title:
           title = title.get_text().encode('utf-8')

        # get the first subtitle
        else:
            title = post.find_all('h2')[0].get_text().encode('utf-8')

        # find the content container
        content = post.find('div', {"class":"entry-content"})

        # parse the content
        text = ' '.join([p.get_text().encode('utf-8') for p in content.find_all('p')])

        # save the article
        self._articles.append(Article(title, text))
