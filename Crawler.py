import requests
import threading
from bs4 import BeautifulSoup
from abc import ABCMeta, abstractmethod

""" Crawler class

A base crawler class for all other site-specific crawlers.

@author         Tycho Atsma  <tycho.atsma@copernica.com>
@file           Crawler.py
@documentation  public
"""
class Crawler(object):

    # define abstract meta class
    __metaclass__ = ABCMeta

    def __init__(self, urls):
        """
        Initialze the crawler with containers for urls,
        articles, and threads.

        @param   array  the urls to crawl
        @return  void
        """
        self._urls = urls
        self._articles = []
        self._threads = []

    def articles(self):
        """
        Get all parsed articles from the urls and
        their contained links.

        @return  Article[]
        """
        # loop over all urls
        for url in self._urls:

            # loop over all links
            for link in self.links(url):

                # start a new thread
                thread = threading.Thread(target = self.article, args=(link,))
                thread.start()

                # save the thread
                self._threads.append(thread)

        # wait for all threads to finish
        for thread in self._threads:
            thread.join()

        # return all articles
        return self._articles

    def soupify(self, url):
        """
        Soupify a given url

        @param   string  the url to soupify
        @return  BeautifulSoup
        """
        print 'soupifying %s' % url
        doc = requests.get(url, proxies={"http":"192.168.1.188"})
        return BeautifulSoup(doc.text.encode('utf-8'))

    @abstractmethod
    def links(self, url):
        """
        Retrieve all page links from a given url

        @param   string  the url to retrieve the links from
        @return  array
        """
        pass

    @abstractmethod
    def article(self, url):
        """
        Retrieve a page's contents, convert it to an Article,
        and add it to the articles

        @param   string  the url of the page
        @return  void
        """
        pass
