from bs4 import BeautifulSoup
import threading
from selenium import webdriver
from time import sleep
from Article import Article
from Crawler import Crawler

""" FrankWatchingParser class

This class provides an interface for retrieving email marketing articles
from their blog.

@author         Tycho Atsma  <tycho.atsma@copernica.com>
@file           FrankWatchingCrawler.py
@documentation  public
"""
class FrankWatchingCrawler(Crawler):

    def start(self):
        """
        Start the selenium chrome driver

        @return  void
        """
        print 'starting driver'
        self.driver = webdriver.Chrome('./chromedriver')
        print 'started driver'

    def close(self):
        """
        Close the selenium chrome driver

        @return  void
        """
        print 'closing driver'
        self.driver.quit()
        print 'driver is closed'

    def page(self, url):
        """
        Initialize the driver with a page
    
        @parm    string  the url to retrieve
        @return  void
        """
        print 'getting page'
        self.driver.get(url)
        sleep(3)
        print '{} is loaded'.format(url)

    def links(self, url):
        """
        Get all article links
    
        @param   string  the url to start with
        @return  array
        """
        # try to get the links
        try:
            # get the page
            self.page(url)

            # go through the cookie wall
            cookie_button = self.driver.find_element_by_xpath('//a[@href="https://www.frankwatching.com/tag/e-mailmarketing/"]')
            cookie_button.click()

            # find all articles
            sleep(3)
            articles = self.driver.find_elements_by_xpath('//div[@class="post-loop post-loop--archive clearfix ng-scope"]')

            # prepare an array of links
            links = []
    
            # loop over the articles
            for article in articles:
                
                # soupify
                soup = BeautifulSoup(article.get_attribute('innerHTML').encode('utf-8'))
                
                # add the link
                links.append(soup.find('a')['href'])

            # return the links
            return links

        # pass exceptions
        except Exception:
                pass

    def article(self, url):
        """
        Get an article's contents given a url

        @param   string  the url to crawl
        @return  void
        """
        print "start crawling %s on thread: %s" % (url, threading.current_thread().name)
        soup = self.soupify(url)

        # get the article container
        article = soup.find('article', {"class":"post"})
    
        # get the title container
        title_container = article.find('div', {"class":"post--title"})
        title = title_container.find('h1').get_text()

        # get the content container
        content = article.find('div', {"class":"content"})
        
        # parse the content
        text = ' '.join([p.get_text().encode('utf-8') for p in content.find_all('p')])

        # return a new article
        self._articles.append(Article(title.encode('utf-8'), text))
