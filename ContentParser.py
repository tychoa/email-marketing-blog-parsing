from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from collections import Counter

# dont allow warnings from unicode
import warnings
warnings.filterwarnings('error', category=UnicodeWarning)

def commons():
    """
    Return all common words utf8 encoded.

    @return  array
    """
    with open('excludes/words.txt', 'r') as f:
        f = f.read()
        return [w.decode('unicode_escape').encode('utf-8') for w in f.split(',')]

# all stopwords
stops = stopwords.words('dutch')

# all common words
common_words = commons()

# all excludes
excludes = stops + common_words

""" Content Parser class

A class that provides an interface to parse online blog articles as .txt
files.

@author         Tycho Atsma  <tycho.atsma@copernica.com>
@file           ContentParser.py
@documentation  public

"""
class ContentParser:

    def __init__(self, text):
        """
        Initialize the content parser with the given text

        @param   string  the content to parse
        @return  void
        """
        self.text = text.replace('-','')
        self.tokenizer = RegexpTokenizer(r'\w+')
        self.tokens = []

    def normalize(self):
        """
        Tokenize, encode to utf-8, and remove all common words.
    
        @return  array
        """
        # tokenize the file
        tokenized = self.tokenizer.tokenize(self.text)

        # prepare an array for the tokens
        tokens = []

        # loop over the tokens
        for token in tokenized:

            # encode the token
            encoded = token.decode('unicode_escape').encode('utf-8').lower()

            # not excluded
            try:
                if encoded not in excludes:
                    tokens.append(encoded)
            except UnicodeWarning:
                pass
        
        # internally save the tokens
        self.tokens = tokens

        # return the parsed tokens
        return tokens

    def count_words(self):
        """
        Count the words of the tokens

        @return  Counter
        """
        # stored tokens?
        if (len(self.tokens) > 1):
            return Counter(self.tokens)
        # normalize and return counted
        else:
            return Counter(self.normalize())
