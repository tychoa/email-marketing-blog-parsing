import threading
from Article import Article
from Crawler import Crawler

""" MarketingFactsCrawler class

This class provides an interface for retrieving email marketing articles
from their blog.

@author         Tycho Atsma  <tycho.atsma@copernica.com>
@file           MarketingFactsCrawler.py
@documentation  public
"""
class MarketingFactsCrawler(Crawler):
    
    def links(self, url):
        """
        Retrieve all article links from a single url

        @param   string  the url name of the page
        @return  array
        """
        soup = self.soupify(url)

        # get the article list
        articles = soup.find_all('li', {"class":"article__item-large"})

        # return a collection with links
        return [article.find('h2').find('a')['href'] for article in articles]

    def article(self, url):
        """
        Retrieve an article's content from a given url

        @param   string  the article url
        @return  Article
        """
        print "start crawling %s on thread: %s" % (url, threading.current_thread().name)
        soup = self.soupify(url)

        # get the page title
        title = [title for title in soup.find_all('h1') if title.get('itemprop') == 'name headline'][0].get_text()

        # get the article body
        body = soup.find('div', {"id":"articleBody"})
        paragraphs = body.find_all('p')

        # prepare a container for the body
        text = ''

        # loop over the paragraphs
        for p in paragraphs:

            # get the text
            text += p.get_text()

        # return the article
        self._articles.append(Article(title.encode('utf-8'), text.encode('utf-8')))
