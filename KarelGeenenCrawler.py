import threading
from Article import Article
from Crawler import Crawler

""" KarelGeenenParser class

This class provides an interface for retrieving email marketing articles
from their blog.

@author         Tycho Atsma  <tycho.atsma@copernica.com>
@file           KarelGeenenCrawler.py
@documentation  public
"""
class KarelGeenenCrawler(Crawler):

    def links(self, url):
        """
        Retrieve all article links from a single url

        @param   string  the url name of the page
        @return  array
        """
        soup = self.soupify(url)

        # get the content section
        content = soup.find_all('div', {"id":"single-one"})
        links = [link.find('a', {"class":"more-link"}).get('href') for link in content if link.find('a', {"class":"more-link"}) is not None]

        # return a collection with links
        return links

    def article(self, url):
        """
        Retrieve an article's content from a given url

        @param   string  the article url
        @return  void
        """
        print "start crawling %s on thread: %s" % (url, threading.current_thread().name)
        soup = self.soupify(url)

        # get the page title
        title = soup.find_all('h1')[0].get_text()

        # get the article body
        body = soup.find('div', {"class":"articleContent"})
        paragraphs = body.find_all('p')

        # prepare a container for the body
        text = ''

        # loop over the paragraphs
        for p in paragraphs:

            # get the text
            text += p.get_text()

        # return the article
        self._articles.append(Article(title.encode('utf-8'), text.encode('utf-8')))
